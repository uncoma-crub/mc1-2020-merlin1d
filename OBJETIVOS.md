### El vehículo de lanzamiento Falcon 9 y su tecnología de propulsión Merlin.

Realizar un trabajo de investigación conjunto en se que se haga un abordaje completo de los siguientes temas:

* Descripción del vehículo de lanzamiento Falcon9, versiones y propósitos.
* Inyector de pinza: principio de funcionamiento y usos.
* Especificaciones del Merlin 1D: partes principales, aspectos tecnicos
* Merlin 1D: Aspectos del diseño y principio de funcionamiento.
* Revisión de las versiones del Merlin 1A a 1D
* Descripción del sistema de contol de aterrizaje del Falcon 9.
* Fases del sistem de c
* Línea de tiempo con logros y fallas del sistema de aterrizaje del Falcon.
* Evaluación de las potenciales demandas de ingeniería en el desarrollo del Falcon y qué materias/conocimientos asociados en su carrera podrían ajustarse más a las mismas.
* Citar la bibliografía

Se sugiere la siguiente bibliografía para comenzar

(spaceX)[https://www.spacex.com/]

(wikipedia: Falcon9 - SpaceX)[https://en.wikipedia.org/wiki/Falcon_9]

(Falcon9 reporte)[http://www.spacelaunchreport.com/falcon9.html#config]

(Inyector de pinza)[https://en.wikipedia.org/wiki/Pintle_injector]

(Inyector de pinza)[https://forum.nasaspaceflight.com/index.php?topic=32983.520]

(wikipedia:  Merlin 1D)[https://en.wikipedia.org/wiki/SpaceX_Merlin]

(wikipedia:  Merlin 1D)[https://en.m.wikipedia.org/wiki/SpaceX_Merlin#Merlin_1D_Vacuum]

(primeras pruebas Merli 1D)[https://www.nasaspaceflight.com/2012/06/spacex-merlin-1d-orbital-fire-aj-26-engine/]

(SpaceX: General Falcon and Dragon discussion)[https://forum.nasaspaceflight.com/index.php?topic=29476.620]

(SpaceX paper on precision landing - and landing technology)[https://forum.nasaspaceflight.com/index.php?topic=41935.0]

(videos de los vuelos de prueba de Falcon9 (F9R) del sitio de Lars Blackmore)[http://www.larsjamesblackmore.com/f9r.htm]

(Seedhouse, Erik. SpaceX's Dragon: America's Next Generation Spacecraft. Springer, 2015.)[ver el Appendix VI]

